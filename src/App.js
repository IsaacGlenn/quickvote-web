import React, { Component } from 'react';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';

// import Router from './components/Router';
import Header from './components/Header';
import userService from './services/userService';
// import { userContext } from './util/appContext';


import Homepage from './pages/Homepage';
import Create from './pages/CreatePoll';
import Register from './pages/Register';
import Login from './pages/Login';
import Confirm from './pages/Confirm';
import NewPassword from './pages/NewPassword';
import ForgotPassword from './pages/ForgotPassword';
import ViewPoll from './pages/ViewPoll';

export class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      user: undefined
    }

    this.logout = this.logout.bind(this);
    this.authorize = this.authorize.bind(this);
  }

  async logout() {
    this.setState({ user: null });
    userService.Logout();
  }

  async authorize() {
    if (await userService.Authorized())
      this.setState({ user: await userService.GetUserDetails(localStorage.getItem('userid')) });
  }

  componentDidMount() {
    // this.authorize();
  }

  render() {
    // const user = {
    //   info: this.state.user || {},
    //   isAuthenticated: this.state.user === undefined ? false : true,
    //   authorize: this.authorize,
    //   logout: this.logout
    // };

    return (
      // <userContext.Provider value={user}>
      //   <div>
      //     <userContext.Consumer>
      //       {(user) => (
      <div>
        <BrowserRouter>
        <Header />
          <Route exact path="/" render={() => (
            <Redirect to="/home" />
          )} />
          <Route path="/home" component={Homepage} />
          <Route path="/create" component={Create} />
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <Route path="/confirm/:token" component={Confirm} />
          <Route path="/user/password/:token" component={NewPassword} />
          <Route path="/user/forgot" component={ForgotPassword} />
          <Route path="/poll/:id" component={ViewPoll} />
        </BrowserRouter>
      </div>
      //         )}
      //       </userContext.Consumer>
      //     </div>
      //   </userContext.Provider>
    );
  }
}

export default App;
