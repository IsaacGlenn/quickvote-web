import React, { Component, Fragment } from 'react';
import Header from '../components/Header';
import userService from '../services/userService';


class Profile extends Component {
    constructor(props) {
        super(props);

        this.state = {

        };

      
    }

    componentDidMount() {
        userService.Authorize();
    }

    render() {
        const { history } = this.props;

        return (
            <Fragment>

                    <section class="hero">
                        <div class="hero-body">
                            <div class="container">
                                <h1 class="title">Email confirmation successful</h1>
                            </div>
                        </div>
                    </section>

                <div class="container">
                    <input type="submit" value="Login" className="button is-primary" onClick={() => history.push('/login')} />
                </div>
            </Fragment>
        );
    }
};

export default Profile;