import React, { Component, Fragment } from 'react';
import pollService from '../services/pollService';
// import swal from 'sweetalert2';

class Homepage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            polls: null,
            error: false
        };
    }

    async componentDidMount() {
        const { polls } = this.state;
        await pollService.ViewActive()
        .then(res =>{
            this.setState({ polls: res });
        })
        .catch(err => {
            this.setState({ error: true });
            console.log('catch')
        });

        if(polls && polls.name === 'SequelizeConnectionRefusedError') 
            this.setState({ polls: null, error: true });
    }

    onSubmit(e, id) {
        const { history } = this.props;
        e.preventDefault();
        history.push(`/poll/${id}`)
    }

    render() {
        const { polls, error } = this.state;

        return (
            <Fragment>
                <div className="container">
                    <div className="columns">
                        <div className="column is-13-tablet">

                        {error ? (
                            <Fragment>
                                <h1>Network error.</h1>
                            </Fragment>
                        ) : (
                            <Fragment>
                                <h1>Available Polls</h1>
                            </Fragment>
                        )}
                            {polls && polls.map((poll) => (

                                <Fragment key={poll.id}>
                                    <br />
                                    <div className="card">
                                        <header className="card-header">
                                            <p className="card-header-title">{poll.PollTitle}</p>
                                        </header>

                                        <div className="card-content">
                                            <form>
                                                <p>{poll.PollDescription}</p>
                                            </form>
                                        </div>

                                        <footer className="card-footer level is-mobile">
                                            <div className="level-left">
                                                <div className="level-item has-text-centered">
                                                    <input type="submit" value="Vote on this" className="button is-primary" onClick={(e) => this.onSubmit(e, poll.id)} />
                                                </div>
                                            </div>
                                        </footer>
                                    </div>
                                </Fragment>
                            ))}

                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
};

export default Homepage;