import React, { Component, Fragment } from 'react';
import { PieChart } from 'react-minimal-pie-chart';
import swal from 'sweetalert2';
import pollService from '../services/pollService';
import Colours from '../util/colours';

class ViewPoll extends Component {
    constructor(props) {
        super(props);

        this.updateSelection = this.updateSelection.bind(this);

        this.state = {
            poll: [],
            pollData: [],
            converted: [],
            categories: [],
            selection: ''
        };
        this.pollID = this.props.match.params.id;
    }

    updateSelection(i){
        const {categories} = this.state; 
        let selection = {
            i: 1
        };
        let selectionList = [];
        
        for(let k = 0; k<categories.length;k++){
            let option = {
                k: 0
            };
            selectionList[k] = option;
        }
        selectionList[i] = selection;
        this.setState({selection: JSON.stringify(selectionList)});
    }

    async componentDidMount() {
        const { history } = this.props;
        let pollData = await pollService.ViewPollData(this.pollID);
        let poll = await pollService.ViewPoll(this.pollID);

        if (!pollData || !poll) {
            swal.fire({
                icon: "error",
                title: "An error occured",
                text: "This poll could not be found!"
            }).then(history.push('../home'));
        } else {
            let pollObj = JSON.parse(pollData.PollJson);
            const pollInfo = {
                id: pollData.id,
                pollID: pollData.PollID
            };
            let data = [];
            let categories = [];

            for (let i = 0; i < Object.values(pollObj).length; i++) {   //Convert data to the format required for the pie chart lib
                let item = { title: i, value: Object.values(pollObj)[i], color: Colours[i] }
                data[i] = item;
            }

            for (let i = 0; i < JSON.parse(poll.PollContent).length; i++) {
                categories[i] = Object.values(JSON.parse(poll.PollContent))[i];
            }

            this.setState({ poll, pollData: pollInfo, converted: data, categories });
        }
    }

    onSubmit(e) {
        const { selection } = this.state;
        const { history } = this.props;
        e.preventDefault();

        if(selection === '') return;
        
        pollService.VoteOnPoll(this.pollID, selection).then(res => {
            swal.fire({
                icon: "success",
                title: "Success",
                text: "Vote recorded!"
            }).then(history.push('../home'));
        });
    }

    render() {
        const { poll, converted, categories } = this.state;
        // console.log(poll);
        return (
            <Fragment>
                <div className="container">
                    <div className="columns">
                        <div className="column is-13-tablet">
                            <p className="">Poll data</p>

                            <div className="card">
                                <header className="card-header">
                                    <p className="card-header-title">{poll.PollTitle}</p>
                                </header>

                                <div className="card-content">
                                    <div className="columns">
                                        <div className="column is-4">
                                            <PieChart
                                                data={converted}
                                                radius='10'
                                            />
                                        </div>
                                    </div>

                                    <form>
                                    <div className="columns">
                                        {categories.map((category) => {
                                            return (
                                            <div className="column auto">
                                                <div className="control" key={category.index}>
                                                    <label className="radio" /><input type="radio" name="answer" onChange={() => this.updateSelection(category.index)}/>{category.value}
                                                </div>
                                                </div>
                                            )
                                        })}
                                        </div>



                                    </form>
                                </div>

                                <footer className="card-footer level is-mobile">
                                    <div className="level-left">
                                        <div className="level-item has-text-centered">
                                            <input type="submit" value="Vote on this" className="button is-primary" onClick={(e) => this.onSubmit(e)} />
                                        </div>
                                    </div>
                                </footer>
                            </div>

                        </div>
                    </div>
                </div>
            </Fragment >
        );
    }
};

export default ViewPoll;