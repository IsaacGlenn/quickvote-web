import React, { Component, Fragment } from 'react';
import * as EmailValidator from 'email-validator';
import swal from 'sweetalert2';
import cfg from '../util/config';
import userService from '../services/userService';

class Register extends Component {
    constructor(props) {
        super(props);

        this.formChanged = this.formChanged.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.validation = this.validation.bind(this);
        this.keypressValidation = this.keypressValidation.bind(this);

        this.backend = `http://${cfg.backend.domain}:${cfg.backend.port}`;

        this.state = {
            username: '',
            forename: '',
            surname: '',
            password: '',
            confirm: '',
            email: ''
        };
    }

    formChanged(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    keypressValidation(e) {
        let regex = null;

        if (e.target.name === 'username') {
            regex = /[a-zA-Z0-9]/g;
        } else if (e.target.name === 'email') {
            regex = /[a-zA-Z0-9@.]/g;
        } else if (e.target.name === 'forename' || e.target.name === 'surname') {
            regex = /[a-zA-Z]/g;
        } else return;

        if (!regex.test(e.key)) {
            e.preventDefault();
        }
    }

    validation() {
        let { username, password, email, confirm } = this.state;

        if (!username || !password || !email || !confirm) return 'All fields are required.';
        if (password !== confirm) return 'Passwords do not match';
        if (!EmailValidator.validate(email)) return 'Email is invalid';
        return true;
    }

    onSubmit(e) {
        e.preventDefault();
        const { username, forename, surname, email, password } = this.state;
        const {history} = this.props;
        const isValid = this.validation();

        if (isValid !== true) {
            swal.fire({
                icon: "error",
                title: "An error occurred",
                text: isValid
            });
        }
        else {
            const account = {
                Username: username,
                Password: password,
                Firstname: forename,
                Lastname: surname,
                EmailAddress: email
            };

            userService.Register(account).then(res => {
                if (!res.data.error) {
                    swal.fire({
                        icon: "success",
                        title: "Account creation successful",
                        text: 'Email verification email has been sent'
                    }).then(result => {
                        if(result.value) history.push('/login');
                    });
                } else {
                    swal.fire({
                        icon: "error",
                        title: "Account creation failed",
                        text: res.data.error
                    });
                }
            }).catch(err => {
                swal.fire({
                    icon: "error",
                    title: "An error occurred",
                    text: err
                });
            });
        }
    }

    render() {
        return (
            <Fragment>
                <br />
                <div className="container">
                    <div className="columns">
                        <div className="column is-13-tablet">

                            <div className="card">
                                <header className="card-header">
                                    <p className="card-header-title">Register an account</p>
                                </header>

                                <div className="card-content">
                                    <form>
                                        <div className="columns">
                                            <div className="column is-4">
                                                <label className="label">Username</label>
                                            </div>
                                            <div className="column">
                                                <div className="control has-icons-left">

                                                    <input className="input is-primary" placeholder="username" type="text" required name="username"
                                                        onChange={(e) => this.formChanged(e)}
                                                        onKeyPress={(e) => this.keypressValidation(e)}
                                                        maxLength="16"
                                                    />
                                                    <span className="icon is-small is-left">
                                                        <i className="fas fa-user"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="columns">
                                            <div className="column is-4">
                                                <label className="label">First name</label>
                                            </div>
                                            <div className="column">
                                                <div className="control has-icons-left">

                                                    <input className="input is-primary" placeholder="Jake" type="text" required name="forename"
                                                        onChange={(e) => this.formChanged(e)}
                                                        onKeyPress={(e) => this.keypressValidation(e)}
                                                        maxLength="16"
                                                    />
                                                    <span className="icon is-small is-left">
                                                        <i className="fas fa-user"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="columns">
                                            <div className="column is-4">
                                                <label className="label">Second name</label>
                                            </div>
                                            <div className="column">
                                                <div className="control has-icons-left">

                                                    <input className="input is-primary" placeholder="Melon" type="text" required name="surname"
                                                        onChange={(e) => this.formChanged(e)}
                                                        onKeyPress={(e) => this.keypressValidation(e)}
                                                        maxLength="16"
                                                    />
                                                    <span className="icon is-small is-left">
                                                        <i className="fas fa-user"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="columns">
                                            <div className="column is-4">
                                                <label className="label">Email address</label>
                                            </div>
                                            <div className="column">
                                                <div className="control has-icons-left">
                                                    <input className="input is-primary" placeholder="someone@email.com" type="text" required name="email"
                                                        onChange={(e) => this.formChanged(e)}
                                                        onKeyPress={(e) => this.keypressValidation(e)}
                                                        maxLength="32"
                                                    />
                                                    <span className="icon is-small is-left">
                                                        <i className="fas fa-envelope"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="columns">
                                            <div className="column is-4">
                                                <label className="label">Password</label>
                                            </div>
                                            <div className="column">
                                                <div className="control has-icons-left">
                                                    <input className="input is-primary" placeholder="********" type="password" required name="password"
                                                        onChange={(e) => this.formChanged(e)}
                                                        maxLength="64"
                                                    />
                                                    <span className="icon is-small is-left">
                                                        <i className="fas fa-lock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="columns">
                                            <div className="column is-4">
                                                <label className="label">Confirm Password</label>
                                            </div>
                                            <div className="column">
                                                <div className="control has-icons-left">
                                                    <input className="input is-primary" placeholder="********" type="password" required name="confirm"
                                                        onChange={(e) => this.formChanged(e)}
                                                        maxLength="64"
                                                    />
                                                    <span className="icon is-small is-left">
                                                        <i className="fas fa-lock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <footer className="card-footer">
                                    <div className="column is-4">
                                        <input type="submit" value="Confirm" className="button is-primary" onClick={(e) => this.onSubmit(e)} />
                                    </div>
                                </footer>

                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }

}

export default Register;