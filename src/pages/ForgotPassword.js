import React, { Component, Fragment } from 'react';
import swal from 'sweetalert2';
import * as EmailValidator from 'email-validator';
import userService from '../services/userService';

class ForgotPassword extends Component {
    constructor(props) {
        super(props);

        this.formChanged = this.formChanged.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.validation = this.validation.bind(this);

        this.state = {
            email: '',
        };

        this.token = this.props.match.params.token;
    }

    formChanged(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    validation() {
        const { email } = this.state;

        if (!EmailValidator.validate(email)) return 'Email is invalid';
        return true;
    }

    keypressValidation(e) {
        let regex = /[a-zA-Z0-9@.]/g;

        if (!regex.test(e.key)) {
            e.preventDefault();
        }
    }

    onSubmit(e) {
        const { email } = this.state;
        e.preventDefault();

        const isValid = this.validation();

        if (isValid !== true) {
            swal.fire({
                icon: "error",
                title: "An error occurred",
                text: isValid
            });
        } else {
            userService.RequestPasswordUpdate(email)
                .then(res => {
                    swal.fire({
                        icon: "success",
                        title: "Password change",
                        text: res
                    });
                });
        }
    }

    render() {
        return (
            <Fragment>
                <br />
                <div className="container">
                    <div className="columns">
                        <div className="column is-13-tablet">

                            <div className="card">
                                <header className="card-header">
                                    <p className="card-header-title">Update your password</p>
                                </header>

                                <div className="card-content">
                                    <form>

                                        <div className="columns">
                                            <div className="column is-4">
                                                <label className="label">Email address</label>
                                            </div>
                                            <div className="column">
                                                <div className="control has-icons-left">
                                                    <input className="input is-primary" placeholder="someone@email.com" type="text" required name="email"
                                                        onChange={(e) => this.formChanged(e)}
                                                        onKeyPress={(e) => this.keypressValidation(e)}
                                                        maxLength="32"
                                                    />
                                                    <span className="icon is-small is-left">
                                                        <i className="fas fa-envelope"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>

                                <footer className="card-footer">
                                    <div className="column is-4">
                                        <input type="submit" value="Confirm" className="button is-primary" onClick={(e) => this.onSubmit(e)} />
                                    </div>
                                </footer>

                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }

}

export default ForgotPassword;