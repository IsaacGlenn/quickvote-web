import React, { Component, Fragment } from 'react';
import swal from 'sweetalert2';
import userService from '../services/userService';

class Login extends Component {
    constructor(props) {
        super(props);

        this.formChanged = this.formChanged.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.validation = this.validation.bind(this);
        this.keypressValidation = this.keypressValidation.bind(this);
        this.forgotPassword = this.forgotPassword.bind(this);

        this.state = {
            username: '',
            password: ''
        };
    }

    formChanged(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    keypressValidation(e) {
        let regex = /[a-zA-Z0-9]/g;
        if (!regex.test(e.key)) e.preventDefault();
    }

    validation() {
        const { username, password } = this.state;

        if (!username || !password) return 'All fields are required';
        return true;
    }

    forgotPassword(){
        this.props.history.push('/user/forgot');
    }

    onSubmit(e) {
        const { username, password } = this.state;
        const { history } = this.props;
        e.preventDefault();

        const isValid = this.validation();

        if (isValid !== true) {
            swal.fire({
                icon: "error",
                title: "An error occurred",
                text: isValid
            });
        } else {
            const account = {
                Username: username,
                Password: password
            };

            userService.Login(account).then(success => {
                if (success) history.push('/home');
                else {
                    swal.fire({
                        icon: "error",
                        title: "Login failed",
                        text: 'Incorrect username or password'
                    });
                }
            }).catch(err => {
                swal.fire({
                    icon: "error",
                    title: "An error occurred",
                    text: err
                });
            });
        }
    }

    render() {
        return (
            <Fragment>
                <br />
                <div className="container">
                    <div className="columns">
                        <div className="column is-13-tablet">

                            <div className="card">
                                <header className="card-header">
                                    <p className="card-header-title">Login to your account</p>
                                </header>

                                <div className="card-content">
                                    <form>
                                        <div className="columns">
                                            <div className="column is-4">
                                                <label className="label">Username</label>
                                            </div>
                                            <div className="column">
                                                <div className="control has-icons-left">

                                                    <input className="input is-primary" placeholder="username" type="text" required name="username"
                                                        onChange={(e) => this.formChanged(e)}
                                                        onKeyPress={(e) => this.keypressValidation(e)}
                                                        maxLength="16"
                                                    />
                                                    <span className="icon is-small is-left">
                                                        <i className="fas fa-user"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="columns">
                                            <div className="column is-4">
                                                <label className="label">Password</label>
                                            </div>
                                            <div className="column">
                                                <div className="control has-icons-left">
                                                    <input className="input is-primary" placeholder="********" type="password" required name="password"
                                                        onChange={(e) => this.formChanged(e)}
                                                        maxLength="64"
                                                    />
                                                    <span className="icon is-small is-left">
                                                        <i className="fas fa-lock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <footer className="card-footer level is-mobile">
                                    <div className="level-left">
                                        <div className="level-item has-text-centered">
                                            <input type="submit" value="Confirm" className="button is-primary" onClick={(e) => this.onSubmit(e)} />
                                        </div>
                                    </div>
                                    <div className="level-right">
                                        <div className="level-item has-text-centered">
                                            <input type="submit" value="Forgot Password" className="button is-primary" onClick={() => this.forgotPassword()} />
                                        </div>
                                    </div>
                                </footer>


                            </div>


                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }

}

export default Login;