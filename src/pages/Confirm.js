import React, { Component, Fragment } from 'react';
import userService from '../services/userService';


class Confirm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            success: false,
            msg: ''
        };

        this.token = this.props.match.params.token;
    }

    componentDidMount() {
        userService.ConfirmEmail(this.token).then(res => {
            if (res.success !== true) this.setState({ success: false, msg: res });
            else this.setState({ success: true })
        });
    }

    render() {
        const { success, msg } = this.state;
        const { history } = this.props;

        return (
            <Fragment>
                {success &&
                    <section class="hero">
                        <div class="hero-body">
                            <div class="container">
                                <h1 class="title">Email confirmation successful</h1>
                            </div>
                        </div>
                    </section>}

                {!success &&
                    <section class="hero">
                        <div class="hero-body">
                            <div class="container">
                                <h1 class="title">Email confirmation failed</h1>
                                <h2 class="subtitle">{msg}</h2>
                            </div>
                        </div>
                    </section>}

                <div class="container">
                    <input type="submit" value="Login" className="button is-primary" onClick={() => history.push('/login')} />
                </div>
            </Fragment>
        );
    }
};

export default Confirm;