import React, { Component, Fragment } from 'react';
import swal from 'sweetalert2';
import userService from '../services/userService';

class NewPassword extends Component {
    constructor(props) {
        super(props);

        this.formChanged = this.formChanged.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.validation = this.validation.bind(this);

        this.state = {
            password: '',
            confirm: '',
        };

        this.token = this.props.match.params.token;
    }

    formChanged(e) {
        this.setState({
			[e.target.name] : e.target.value
        });
    }

    validation(){
        const { password, confirm } = this.state;

        if(!password || !confirm) return 'All fields are required';
        if(password.length < 6) return 'Password must be longer than 6 characters';
        if(password !== confirm) return 'Passwords do not match';
        return true;
    }

    onSubmit(e) {
        const {password} = this.state;
        const {history} = this.props;
        e.preventDefault();

        const isValid = this.validation();

        if (isValid !== true) {
            swal.fire({
				icon: "error",
				title: "An error occurred",
				text: isValid
			});
        } else {
            userService.UpdatePassword(this.token, password)
            .then(status => {
                if(status === true){
                    swal.fire({
                        icon: "success",
                        title: "Password changed successfully",
                        text: 'Success'
                    });
                    history.push("/login");
                } else {
                    swal.fire({
                        icon: "error",
                        title: "Password change failed",
                        text: status
                    });
                }
            });
        }   
    }

    render() {
        return (
            <Fragment>
                <br />
                <div className="container">
                    <div className="columns">
                        <div className="column is-13-tablet">

                            <div className="card">
                                <header className="card-header">
                                    <p className="card-header-title">Update your password</p>
                                </header>

                                <div className="card-content">
                                    <form>
                                        <div className="columns">
                                            <div className="column is-4">
                                                <label className="label">Password</label>
                                            </div>
                                            <div className="column">
                                                <div className="control has-icons-left">
                                                    <input className="input is-primary" placeholder="********" type="password" required name="password"
                                                        onChange={(e) => this.formChanged(e)} 
                                                        maxLength="64"
                                                     />
                                                    <span className="icon is-small is-left">
                                                        <i className="fas fa-lock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="columns">
                                            <div className="column is-4">
                                                <label className="label">Confirm Password</label>
                                            </div>
                                            <div className="column">
                                                <div className="control has-icons-left">
                                                    <input className="input is-primary" placeholder="********" type="password" required name="confirm" 
                                                        onChange={(e) => this.formChanged(e)} 
                                                        maxLength="64"    
                                                    />
                                                    <span className="icon is-small is-left">
                                                        <i className="fas fa-lock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <footer className="card-footer">
                                    <div className="column is-4">
                                        <input type="submit" value="Confirm" className="button is-primary" onClick={(e) => this.onSubmit(e)} />
                                    </div>
                                </footer>

                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }

}

export default NewPassword;