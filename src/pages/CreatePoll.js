import React, { Component, Fragment } from 'react';
import Select from 'react-select';
import swal from 'sweetalert2';
import pollService from '../services/pollService';
import userService from '../services/userService';

const Option = (props) => {
    return (
        <div className="columns">
            <div className="column is-4">
                <label className="label">Option {props.index + 1}</label>
            </div>
            <div className="column">
                <div className="control">
                    <input className="input is-primary" placeholder="Option title" type="text" required name={`option${props.index}`} onChange={(e) => props.updateList(props.index, e)} />
                </div>
            </div>
        </div>
    );
};

class CreatePoll extends Component {
    constructor(props) {
        super(props);

        this.addOption = this.addOption.bind(this);
        this.removeOption = this.removeOption.bind(this);
        this.formChanged = this.formChanged.bind(this);
        this.updateList = this.updateList.bind(this);
        this.checboxChanged = this.checboxChanged.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            title: '',
            description: '',
            optionList: [],
            allowAnonymous: false,
            duration: 0,
            options: [
                { value: '30', label: '30 mins' },
                { value: '60', label: '1 hour' },
                { value: '120', label: '2 hours' },
                { value: '180', label: '3 hours' },
                { value: '360', label: '6 hours' }]
        };
    }

    formChanged(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    selectChanged(e) {
        this.setState({ duration: e.value });
    }

    checboxChanged() {
        const { allowAnonymous } = this.state;
        this.setState({ allowAnonymous: !allowAnonymous });
    }

    addOption() {
        let { optionList } = this.state;
        let optionCount = optionList.length;

        if (optionCount >= 10) return;

        let option = {
            index: optionCount,
            value: ''
        };
        optionList[optionCount] = option;
        this.setState({ optionList });
    }

    removeOption() {
        let { optionList } = this.state;
        let optionCount = optionList.length;

        if (optionCount === 2) return;

        optionList.pop();
        this.setState({ optionList });
    }

    updateList(index, e) {
        let { optionList } = this.state;
        optionList[index].value = e.target.value;

        this.setState({ optionList });
    }

    componentDidMount() {
        userService.Authorized().then(res => {
            if(!res){
                swal.fire({
                    icon: "error",
                    title: "Unauthorized",
                    text: 'You must be logged in to post a Poll'
                }).then(this.props.history.push('/login'));
            }
        }).catch(err => {
            swal.fire({
                icon: "error",
                title: "An error occurred",
                text: err
            });
            this.props.history.push('/home')
        });

        let { optionList } = this.state;
        for (let i = 0; i <= 1; i++) {
            let option = {
                index: i,
                value: ''
            };
            optionList[i] = option;
        }
        this.setState({ optionList });
    }

    async onSubmit(e) {
        e.preventDefault();
        userService.Authorized().then(res => {
            if(!res){
                swal.fire({
                    icon: "error",
                    title: "Unauthorized",
                    text: 'You must be logged in to post a Poll'
                }).then(this.props.history.push('/login'));
            }
        }).catch(err => {
            swal.fire({
                icon: "error",
                title: "An error occurred",
                text: err
            }).then(this.props.history.push('/home'));;
            return;
        });

        const userid = await localStorage.getItem('userid');
        const { title, description, optionList, allowAnonymous, duration } = this.state;

        const FieldCount = optionList.length;
        const poll = {
            PollTitle: title,
            PollContent: JSON.stringify(optionList),
            IsActive: true,
            Duration: duration,
            CreatedBy: userid,
            PollDescription: description,
            AllowAnonymous: allowAnonymous
        };

        pollService.CreatePoll(poll, FieldCount).then(res => {
            if (res.errors) {
                swal.fire({
                    icon: "error",
                    title: "An error occured",
                    text: "Please refresh the page and try again!"
                });
            } else {
                swal.fire({
                    icon: "success",
                    title: "Poll created successfully",
                    text: res.data
                }).then(this.props.history.push('/home'));
            }
        }).catch(err => {
            swal.fire({
                icon: "error",
                title: "An error occurred",
                text: err
            });
        });
    }

    render() {
        const { optionList, options } = this.state;

        return (
            <Fragment>
                <br />
                <div className="container">
                    <div className="columns">
                        <div className="column is-13-tablet">

                            <div className="card">
                                <header className="card-header">
                                    <p className="card-header-title">Create a Poll</p>
                                </header>

                                <div className="card-content">
                                    <form>

                                        <div className="columns">
                                            <div className="column is-4">
                                                <label className="label">Title</label>
                                            </div>
                                            <div className="column">
                                                <div className="control">
                                                    <input className="input is-primary" placeholder="Poll title" type="text" required name="title" onChange={(e) => this.formChanged(e)} />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="columns">
                                            <div className="column is-4">
                                                <label className="label">Description</label>
                                            </div>
                                            <div className="column">
                                                <div className="control">
                                                    <textarea className="textarea is-primary" placeholder="More information about your poll" rows="5" name="description" onChange={(e) => this.formChanged(e)}></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        {optionList.map((option) => {
                                            return (
                                                <Option
                                                    key={option.index}
                                                    index={option.index}
                                                    updateList={this.updateList}
                                                />
                                            )
                                        })}

                                        <div className="columns">
                                            <div className="column is-three-quarters"></div>
                                            <div className="column">
                                                <div className="control">
                                                    <input className="button is-primary" value="Add option" type="submit" onClick={(() => this.addOption())} />
                                                </div>
                                            </div>
                                            <div className="column">
                                                <div className="control">
                                                    <input className="button is-primary" value="Remove option" type="submit" onClick={(() => this.removeOption())} />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="columns">
                                            <div className="column is-4">
                                                <label className="label">Poll duration</label>
                                            </div>
                                            <div className="column">
                                                <div className="control">
                                                    <Select options={options} onChange={(e) => this.selectChanged(e)} />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="columns">
                                            <div className="column is-4">
                                                <label className="label">Account required to vote (recommended)</label>
                                            </div>
                                            <div className="column">
                                                <div className="control">
                                                    <input type="checkbox" className="is-primary" name="allowAnonymous" defaultChecked="true" onClick={() => this.checboxChanged()} />
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                </div>

                                <footer className="card-footer">
                                    <div className="column is-4">
                                        <input type="submit" value="Create" className="button is-primary" onClick={(e) => this.onSubmit(e)} />
                                    </div>
                                </footer>

                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default CreatePoll;