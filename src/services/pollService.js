import cfg from '../config';
import Axios from 'axios';

const url = `http://${cfg.backend.domain}:${cfg.backend.port}/polls`;
const data = `http://${cfg.backend.domain}:${cfg.backend.port}/polldata`;

const pollService = {
    ViewActive() {
        return Axios.get(`${url}/active`).then(res => {
            return res.data;
        });
    },
    ViewPoll(id) {
        return Axios.get(`${url}/id/${id}`).then(res => {
            return res.data;
        })
    },
    CreatePoll(poll, FieldCount) {
        return Axios.put(`${url}/new`, poll).then(res => {
            this.CreatePollData(res.data.id, FieldCount);
            return res.data;
        });
    },
    ViewPollData(id) {
        return Axios.get(`${data}/id/${id}`).then(res => {
            return res.data;
        });
    },
    CreatePollData(id, fc) {
        let item = { PollID: id, FieldCount: fc };
        return Axios.put(`${data}/new`, item).then(res => {
            return res.data;
        });
    },
    VoteOnPoll(id, info){
        let item = {PollJson: info};
        return Axios.post(`${data}/id/${id}`, item).then(res => {
            return res.data;
        });
    }
};

export default pollService;