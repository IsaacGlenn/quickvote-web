import cfg from '../config';
import Axios from 'axios';

const url = `http://${cfg.backend.domain}:${cfg.backend.port}/users`;

const userService = {
    UpdatePassword(token, pass) {
        return Axios.post(`${url}/update/${token}`, { password: pass })
            .then(res => {
                if (res.data.success) return true;
                else return res.data.message;
            })
    },
    RequestPasswordUpdate(email) {
        return Axios.post(`${url}/forgot`, { email: email })
            .then(res => {
                return res.data;
            });
    },
    Login(account) {
        return Axios.post(`${url}/login`, account)
            .then(res => {
                if (res.data.token) {
                    localStorage.setItem('login', res.data.token);
                    localStorage.setItem('userid', res.data.id);
                    return true;
                } else return false;
            });
    },
    Logout(){
        localStorage.removeItem('login');
        localStorage.removeItem('userid');
    },
    Register(account) {
        return Axios.put(`${url}/register`, account)
            .then(res => {
                return res;
            })
    },
    ConfirmEmail(token) {
        return Axios.get(`${url}/confirmation/${token}`)
            .then(res => {
                if (res.data.success) return true;
                else return res.data.message;
            });
    },
    async Authorized() {
        const token = localStorage.getItem('login');
        const res = await Axios.get(`${url}/validate/${token}`);

        if(res.data.success) return true;
        else return false;
    },
    GetUserDetails(id){
        return Axios.get(`${url}/view/${id}`).then(res => {
            return res.data;
        });
    }
};

export default userService;