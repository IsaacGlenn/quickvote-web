import React, { Component } from "react";
import { BrowserRouter, Route, Redirect } from 'react-router-dom';

import Homepage from '../pages/Homepage';
import Create from '../pages/CreatePoll';
import Register from '../pages/Register';
import Login from '../pages/Login';
import Confirm from '../pages/Confirm';
import NewPassword from '../pages/NewPassword';
import ForgotPassword from '../pages/ForgotPassword';
import ViewPoll from '../pages/ViewPoll';

// const AuthenticatedRoute = ({ component : Component, user, admin, ...rest }) => {
// 	return (
// 		<Route {...rest} render={props => {
// 			return (
// 				<userContext.Consumer>
// 					{user => user.isAuthenticated && (admin ? user.details.admin : true) ?
// 						(<Component {...props} />) : user.isAuthenticated === undefined ? (<div/>) : (<Redirect to="/"/>)
// 					}
// 				</userContext.Consumer>
// 			);
// 		}}/>
// 	);
// };

class Router extends Component {
    // constructor(props) {
    //     super(props);
    
    // }


    render() {
      return(
        <BrowserRouter>
        <Route exact path="/" render={() => (
          <Redirect to="/home" />
        )} />
        <Route path="/home" component={Homepage} />
        <Route path="/create" component={Create} />
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <Route path="/confirm/:token" component={Confirm} />
        <Route path="/user/password/:token" component={NewPassword} />
        <Route path="/user/forgot" component={ForgotPassword} />
        <Route path="/poll/:id" component={ViewPoll} />
      </BrowserRouter>
      );
    }
}

export default Router;