import React, { Component, Fragment } from "react";
import {Link, withRouter} from "react-router-dom";
import { Navbar } from "react-bulma-components";
import userService from '../services/userService';
import swal from "sweetalert2";

class Header extends Component {
    constructor(props) {
        super(props);
    
		this.toggle = this.toggle.bind(this);
        
		this.state = {
            isOpen: false,
		};
    }

    async componentDidMount(){
        userService.Authorized().then(res => {
            if(!res) this.setState({ isAuthenticated: false });
            else this.setState({ isAuthenticated: true });
        }).catch(err => {
            this.setState({ isAuthenticated: false });
        });
    }

    toggle() {
		this.setState({
			isOpen: !this.state.isOpen
		});
	}

    logout(){
        swal.fire({
            icon: "warning",
            title: 'Logout',
            text: 'Are you sure you want to log out?',
            showCancelButton: true
        }).then((res) => {
            if (res.isConfirmed){
                userService.Logout();
                swal.fire({
                    icon: "success",
                    title: 'Logout',
                    text: 'Logged out'
                })
            }
        }).catch(err => {
          console.log(err);      
        });
    }

    render() {
        const { isOpen, isAuthenticated } = this.state;

        return (
            <Navbar
            color="primary"
            active={isOpen}
        >
        <div className="container">

            <Navbar.Brand>
                <Navbar.Item renderAs={Link} to="/">
                    <b>QuickVote</b>
                </Navbar.Item>
                <Navbar.Burger
                    onClick={() => this.toggle()}
                />
            </Navbar.Brand>
            <Navbar.Menu>
                <Navbar.Container>
                    <Link className="navbar-item" to="/">Home</Link>
                    {isAuthenticated ?<Link to="/create">Create Poll</Link>:<div></div> }
                </Navbar.Container>
                <Navbar.Container position="end">
                    {isAuthenticated ? 
                        <Fragment>
                        <Link className="navbar-item" to="/profile">Profile</Link>
                        <a className="navbar-item" onClick={() => this.logout()}>Logout</a>
                        
                        </Fragment>
                        : <Fragment>
                            <Link to="/login" className="navbar-item">Login</Link>
                            <Link to="/register" className="navbar-item">Register</Link>
                        </Fragment>}
                </Navbar.Container>
            </Navbar.Menu>
            </div>
        </Navbar>
        );
    }
}

export default withRouter(Header);