const Colours = [
    '#6B14EB',
    '#EB1428',
    '#94EB14',
    '#14EBD7',
    '#2F4ED0',
    '#D02F9F',
    '#D0B12F',
    '#2FD060',
    '#C53E3A',
    '#F50ACD'
];

module.exports = Colours; 